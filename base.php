<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if lt IE 8]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>
    <div class="wrap container" role="document">
      <div class="content row">
        <main class="main">
          <?php include Wrapper\template_path(); ?>
        </main><!-- /.main -->
        <?php if (Setup\display_sidebar()) : ?>
          <?php if ( Wrapper\sidebar_path_move_to_bottom() ): ?>
            <aside class="sidebar move-to-bottom">
              <?php include Wrapper\sidebar_path(); ?>
            </aside><!-- /.sidebar .move-to-bottom -->
          <?php elseif( Wrapper\sidebar_path_move_to_top() ): ?>
            <aside class="sidebar move-to-top">
              <?php include Wrapper\sidebar_path_move_to_top(); ?>
            </aside><!-- /.sidebar .move-to-top -->
          <?php endif; ?>
        <?php endif; ?>
      </div><!-- /.content -->
    </div><!-- /.wrap -->
    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
  </body>
</html>
