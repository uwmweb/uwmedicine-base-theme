<?php
/**
 * Template Name: Advisory Board Form
 */
?>
<?php \Roots\Sage\Setup\define_current_template('page-advisory-board-form.php'); ?>

<?php if ( function_exists('yoast_breadcrumb') )
{yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.js"></script>
<script src="<?php echo \Roots\Sage\Assets\asset_path('scripts/advisory-board-subscribe.js') ?>"></script>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>

<!-- advisoryboard subscription form -->
<script>
(function(e, t, n, a, c) {a = e.createElement(t), c = e.getElementsByTagName(t)[0], a.async = 1, a.src = n, c.parentNode.insertBefore(a, c)})(document, "script", "https://mktgsres.advisory.com/resources/lp_snippet/prod/lplib.js");
window.addEventListener ? window.addEventListener("load", init, false) : window.attachEvent && window.attachEvent("onload", init);

function init() {
  lplib.set('uw_med_pb7', 'index.php');
  lplib.sendReferrer();
}
</script>
<!-- /advisoryboard subscription form  -->
