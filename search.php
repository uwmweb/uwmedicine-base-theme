<div class="page-header">
  <h1>Search Results</h1>
</div>
<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif ?>

<?php while (have_posts()) : the_post(); ?>
  <?php $search_type = "search-" . get_post_type(); ?>
  <?php get_template_part('templates/content', $search_type); ?>
<?php endwhile; ?>

<?php the_posts_pagination( array(
                                'prev_text'          => __( '&laquo; Previous <span class="hidden-xs">Results</span>', 'sage' ),
                                'next_text'          => __( '<span class="visible-xs-inline-block">More &raquo;</span><span class="hidden-xs">More Results &raquo;</span>', 'sage' ),
                                'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'sage' ) . ' </span>',
                            ) );
  ?>
