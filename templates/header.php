<header class="banner">
  <div class="container">
    <div class="logo">
      <a href="<?= esc_url(home_url('/')); ?>">
        <img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/logo.png" srcset="<?php echo get_stylesheet_directory_uri() ?>/dist/images/logo.png 1x, <?php echo get_stylesheet_directory_uri() ?>/dist/images/logo-2x.png 2x" alt="UW Medicine Northwest Hospital Logo" />
      </a>
    </div>
    <div class="header-right">
      <?php get_template_part('templates/header-right'); ?>
    </div>
  </div>
</header>
<hr class="header-separator" />
<nav class="nav-primary" role="navigation">
  <div class="container">
    <?php get_template_part('templates/nav-primary'); ?>
  </div>
</nav>
