<article <?php post_class(); ?>>
  <header>
    <?php $parent_heading = get_the_title($post->post_parent);
    $parent_link = get_permalink($post->post_parent); ?>
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <?php if( $post->post_parent ) : ?>
      <p class="search-filed-under">Filed Under: <a href="<?php echo $parent_link ?>"><?php echo $parent_heading ?></a></p>
    <?php endif ?>
    <?php if (get_post_type() === 'post') { get_template_part('templates/entry-meta'); } ?>
  </header>
  <div class="entry-summary">
    <?php the_excerpt(); ?>
  </div>
</article>
