jQuery(function() {

  jQuery('#other').change(function () { // toggle visibility of 'other_text_input' list item
     jQuery('.other_text_input').toggle(this.checked);
  }).change(); //ensure visible state matches initially

  jQuery("#form").validate({ // validate form
    rules: {
      firstName: {
        required: true
      },
      lastName: {
        required: true
      },
      email: {
        required: true,
        email: true
      },
      other_text: {
        required: '#other:checked'
      },
    },
    messages: {
      firstName: {
        required: "Please enter your first name"
      },
      lastName: {
        required: "Please enter your last name"
      },
      email: {
        required: "Please enter your email",
        email: "Please enter a valid email"
      },
      other_text: {
        required: "Please enter a value"
      }
    },
    submitHandler: function(e) {
      lplib.submitForm('#form', function() {});
      jQuery('#submitForm').fadeOut();
      setTimeout(function() {
        jQuery('#formFlash').append('<p>Thank you for subscribing!</p>');
      }, 1000);
      return false;
    }
  });
});

jQuery(function () {

});
